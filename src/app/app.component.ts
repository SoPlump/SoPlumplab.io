import { AfterViewInit, Component } from '@angular/core';
import 'jarallax';
declare var jarallax: any;

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit {
  title = 'portfolio';

  ngAfterViewInit() {
    jarallax(document.querySelectorAll('.jarallax'), {
      speed: 0.2
    });
  }

  constructor(private translate: TranslateService) {
    translate.setDefaultLang('fr');
  }
}
