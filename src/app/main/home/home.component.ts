import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  constructor() { }

  ngOnInit(): void {

  }
  pdfSrc = "../../assets/pdf/SophieRaudrant_CV.pdf";

  ngAfterViewInit() {
    //Copy in all the js code from the script.js. Typescript will complain but it works just fine
  }
}