import { Component, OnInit } from '@angular/core';

import { GameList, SoftwareList } from "../../../../assets/data/project-list";
import { ProjectData } from "../../models/project-data.model";


@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnInit {

  constructor() { }

  gameList: Array<ProjectData> = GameList;
  softwareList: Array<ProjectData> = SoftwareList;

  ngOnInit(): void {
  }
}
