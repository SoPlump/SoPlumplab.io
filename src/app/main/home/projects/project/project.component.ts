import { Component, Input, OnInit } from '@angular/core';
import { ProjectData } from 'src/app/main/models/project-data.model';

// Modal dialog
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ModalComponent } from '../modal/modal.component';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.scss']
})
export class ProjectComponent implements OnInit {

  @Input('projectData') projectData: ProjectData;

  constructor(public matDialog: MatDialog) { }

  ngOnInit(): void {
  }

  openModal() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.id = "modal-component";
    dialogConfig.maxHeight = "80vh";
    dialogConfig.width="80%";
    dialogConfig.data = this.projectData;

    const modalDialog = this.matDialog.open(ModalComponent, dialogConfig);
  }
}
