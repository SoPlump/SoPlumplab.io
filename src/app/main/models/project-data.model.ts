export class ProjectData {
    name: string;
    links: Array<ProjectLink>;
    images: Array<Image>;
    videos: Array<string>;
}

// logo can be a favicon or an image
export class ProjectLink {
    href: string;
    favicon: string;
    image: string;
}

export class Image {
    src: string;
}