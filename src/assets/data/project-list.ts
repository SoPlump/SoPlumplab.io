import { ProjectData } from "../../app/main/models/project-data.model"

export const GameList: Array<ProjectData> = [
    {
        name: "spooknsplash",
        links: [
            {
                href: "https://thomas-guitton.itch.io/spookn-splash",
                favicon: "fab fa-itch-io",
                image: ""
            },
            {
                href: "https://www.lequotidien.com/actualites/competition-ubisoft-spook-n-splash-de-luqac-rafle-deux-prix-24205a85273e97c4af80805ebec35912",
                favicon: "",
                image: "assets/images/lequotidien.png"
            },
            {
                href: "https://montreal.ubisoft.com/fr/nos-engagements/education/concours-universitaire/",
                favicon: "",
                image: "assets/images/ubisoft.png"
            }
        ],
        images: [
            {
                src:"",
            },
            {
                src:"",
            },
            {
                src:"",
            }
        ],
        videos:[
            "",
        ]
    },
    {
        name: "maskmaker",
        links: [
            {
                href: "http://www.innerspacevr.com/",
                favicon: "",
                image: "assets/images/innerspacevr.png"
            },
            {
                href: "https://blog.vive.com/us/2020/11/20/behind-mask-chat-developers-maskmaker/",
                favicon: "",
                image: "assets/images/vive.png"
            },
            {
                href: "https://store.steampowered.com/app/1337530/Maskmaker/",
                favicon: "",
                image: "assets/images/steam.png"
            }
        ],
        images: [
            {
                src:"",
            },
            {
                src:"",
            },
            {
                src:"",
            },
            {
                src:"",
            },
            {
                src:"",
            },
            {
                src:"",
            },
            {
                src:"",
            }
        ],
        videos:[
            "",
        ]
    },
    {
        name: "lazarus",
        links: [
            {
                href: "https://soplump.itch.io/lazarus",
                favicon: "fab fa-itch-io",
                image: ""
            },
            {
                href: "https://gitlab.com/SoPlump/mini-gamejam-72",
                favicon: "fab fa-gitlab",
                image: ""
            }
        ],
        images: [
            {
                src:"",
            },
            {
                src:"",
            },
            {
                src:"",
            },
            {
                src:"",
            },
            {
                src:"",
            }
        ],
        videos:[
            "",
        ]
    },
    {
        name: "la-folie-de-brice",
        links: [
            {
                href: "https://soplump.itch.io/la-folie-de-brice",
                favicon: "fab fa-itch-io",
                image: ""
            },
            {
                href: "https://gitlab.com/SoPlump/la-folie-de-brice",
                favicon: "fab fa-gitlab",
                image: ""
            }
        ],
        images: [
            {
                src:"",
            },
            {
                src:"",
            },
            {
                src:"",
            },
            {
                src:"",
            }
        ],
        videos:[
        ]
    },
    {
        name: "breumage",
        links: [
            {
                href: "https://soplump.itch.io/breumage",
                favicon: "fab fa-itch-io",
                image: ""
            },
            {
                href: "https://gitlab.com/SoPlump/wonderjam_uqac2020s1",
                favicon: "fab fa-gitlab",
                image: ""
            }
        ],
        images: [
            {
                src:"",
            },
            {
                src:"",
            }
        ],
        videos:[
        ]
    }
    // {
    //     name: "escape-the-room",
    //     links: [
    //         {
    //             href: "https://www.youtube.com/watch?v=uEEoCJ1itXk&feature=youtu.be",
    //             favicon: "",
    //             image: "assets/images/youtubeLogo.png"
    //         }
    //     ]
    // }
]

export const SoftwareList: Array<ProjectData> = [
    {
    name: "video-game-engine",
    links: [
        {
            href: "https://gitlab.com/SoPlump/videos-game-engine",
            favicon: "fab fa-gitlab",
            image: ""
        }
    ],
    images: [
        {
            src:"",
        }
    ],
    videos:[
    ]
    },
    {
    name: "compiler",
    links: [
        {
            href: "https://gitlab.com/htpps/compilateur-c",
            favicon: "fab fa-gitlab",
            image: ""
        }
    ],
    images: [
        {
            src:"",
        },
        {
            src:"",
        }
    ],
    videos:[
    ]
    },
    {
    name: "croix-rouge",
    links: [
        {
            href: "https://gestapp.dd69-primus.fr/",
            favicon: "",
            image: "assets/images/croixRougeLogo.jpeg"
        },
        {
            href: "https://gitlab.com/sia-insa-lyon/dev/dev-croix-rouge",
            favicon: "fab fa-gitlab",
            image: ""
        }
    ],
    images: [
        {
            src:"",
        },
        {
            src:"",
        }
    ],
    videos:[
    ]
    }
]